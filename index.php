<!DOCTYPE html>
<html lang="sr">
    <head>
        <meta charset="UTF-8">
        <title>Пријави се</title>
        <link rel="stylesheet" href="css/style.css">
    </head>

    <body>
        <form action="php_servers/log_in.php" method="post" class="box" id="acc_form">
            <div id="heading">ПРИЈАВИ СЕ</div>

            <input type="text" class="input" id="username_input" name="username_input" placeholder="Корисничко име">
            <br> <br>
            <input type="password" class="input" id="password_input" name="password_input" placeholder="Лозинка">
            <br> <br>
            <input type="submit" value="Потврди" id="submit_button" name="submit_button">
            <br> <br>
            <a href="html/make_account.php">Немаш налог? Направи га</a>
            <br> <br>
            <p id="error">
                <?php
                    if(isset($_GET["error"])) {
                        $error = $_GET["error"];

                        echo "ГРЕШКА: $error, ";

                        switch ($error) {
                            case 1:
                                echo "Нисте уписали податке";
                                break;
                            case 2:
                                echo "Корисничко име или лозинка је нетачна";
                                break;
                            default:
                                echo "Непозната грешка";
                                break;
                        }
                    }
                ?>
            </p>
        </form>
    </body>
</html>