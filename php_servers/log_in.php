<?php
    if (isset($_POST["submit_button"])) {
        if (empty($_POST["username_input"]) || empty($_POST["password_input"]))
            header("Location: ../index.php?error=1");
        else {
            $user = $_POST["username_input"];
            $pass = $_POST["password_input"];

            $conn = mysqli_connect("localhost", "root", "");
            $db = mysqli_select_db($conn, "main_database");
            if($db == false)
                echo "<script> alert(\"ГРЕШКА: Није се могуће повезати за базом података... \"); </script>";

            $query = "SELECT * FROM log_in_info WHERE userPassword = '$pass' AND userName = '$user';";
            $result = mysqli_query($conn, $query);
            $rows = mysqli_fetch_array($result);

            if (mysqli_num_rows($result) > 0) {
                session_start();

                $_SESSION["username"] = $user;

                header("Location: ../html/logged_in.php");
                exit;
            } else
                header("Location: ../index.php?error=2");

            mysqli_close($conn);
        }
    }
