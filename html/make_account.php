<?php
    include("make_account.php");
?>

<!DOCTYPE html>
<html lang="sr">
    <head>
        <meta charset="UTF-8">
        <title>Направи профил</title>
        <link rel="stylesheet" href="../makeacc_page/style.css">
    </head>

    <body>
        <form action="" method="post" class="box" id="makeacc_form">
            <div id="heading">Направи профил</div>

            <input type="text" class="input" id="username_input" name="username_input" placeholder="Корисничко име">
            <br> <br>
            <input type="password" class="input" id="password_input" name="password_input" placeholder="Лозинка">
            <br> <br>
            <input type="submit" value="Potvrdi" id="submit_button" name="submit_button">
            <br> <br>
        </form>
    </body>
</html>